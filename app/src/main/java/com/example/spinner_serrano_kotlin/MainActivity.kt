package com.example.spinner_serrano_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Spinner
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var spnPaisess: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spinner = findViewById<Spinner>(R.id.spnPaises)
        val lista = resources.getStringArray(R.array.paises)
        val adaptador = ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, lista)
        spinner.adapter = adaptador

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                Toast.makeText(this@MainActivity, lista[position], Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO: Implementa lo que hacer cuando no se selecciona nada
            }
        }

        spnPaisess = findViewById(R.id.spnPaisess)
        val adaptadorListView = ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, lista)
        spnPaisess.adapter = adaptadorListView

        spnPaisess.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(
                this@MainActivity,
                "Seleccionó el país ${adapterView.getItemAtPosition(i).toString()}",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}